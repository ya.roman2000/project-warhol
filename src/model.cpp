//
// Created by roman on 9/30/18.
//

#include "model.h"
#include "a1/objloader.h"


model::model(std::string path) {

    loadOBJ(path.c_str() , vertices, normals, uv);
    glGenVertexArrays(1, &VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0])*vertices.size(), &vertices[0], GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);
//    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    glEnableVertexAttribArray(0);

}

std::vector<glm::vec3> model::get_vertices(){
    return vertices;
}

GLuint model::get_VAO() {
    return VAO;
}

GLuint model::get_VBO() {
    return VBO;
}

unsigned long model::get_size() {
    return vertices.size();
}

void model::draw(std::vector<DRAW> draw_type) {

    glBindVertexArray(VBO);
    for(DRAW type :draw_type){
        switch(type){
            case DRAW::POINTS :
                glDrawArrays(GL_POINTS, 0, vertices.size());
                break;
            case DRAW::LINES :
                glDrawArrays(GL_LINES, 0, vertices.size());
                break;
            case DRAW::FACES :
                glDrawArrays(GL_TRIANGLES, 0, vertices.size());
                break;
        }
    }
}



