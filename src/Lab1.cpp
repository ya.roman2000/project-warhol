//Lab 1
//modified from http://learnopengl.com/

#include "GL/glew.h"	// include GL Extension Wrangler
#include "GLFW/glfw3.h"	// include GLFW helper library
#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include "a1/shaloader.h"
#include "glm/gtc/matrix_transform.hpp"
#include "model.h"

using namespace std;

// Window dimensions
const GLuint WIDTH = 800, HEIGHT = 800;
static glm::vec3 campos = glm::vec3(0, 0, -5);
static glm::vec3 camdir = glm::vec3(0, 0, 1);
static glm::vec3 camup = glm::vec3(0, 1, 0);

static glm::mat4 model_matrix = glm::mat4(1.0f);
static glm::mat4 translate_matrix = glm::mat4(1.0f);
static glm::mat4 scale_matrix = glm::mat4(1.0f);
static glm::mat4 rotate_matrix = glm::mat4(1.0f);

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{

    if (key == GLFW_KEY_O && action == GLFW_PRESS)
        scale_matrix = glm::scale(scale_matrix,glm::vec3(1.1f,1.1f,1.1f));
    if (key == GLFW_KEY_P && action == GLFW_PRESS)
        scale_matrix = glm::scale(scale_matrix, glm::vec3(0.9f,0.9f,0.9f));

    if (key == GLFW_KEY_B && action == GLFW_PRESS)
        rotate_matrix = glm::rotate(rotate_matrix,glm::radians(1.1f), glm::vec3(1.0f,0,0));
    if (key == GLFW_KEY_N && action == GLFW_PRESS)
        rotate_matrix = glm::rotate(rotate_matrix,glm::radians(1.1f), glm::vec3(0,1.0f,0));
    if (key == GLFW_KEY_E && action == GLFW_PRESS)
        rotate_matrix = glm::rotate(rotate_matrix,glm::radians(1.1f), glm::vec3(0,0,1.0f));

    if (key == GLFW_KEY_J && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(1.0f,0,0));
    if (key == GLFW_KEY_L && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(-1.0f,0,0));

    if (key == GLFW_KEY_I && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(0,1.0f,0));
    if (key == GLFW_KEY_K && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(0,-1.0f,0));

    if (key == GLFW_KEY_PAGE_UP && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(0,0,1.0f));
    if (key == GLFW_KEY_PAGE_DOWN && action == GLFW_PRESS)
        translate_matrix = glm::translate(translate_matrix,glm::vec3(0,0,-1.0f));

//    if(key == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
//        mouseState = MouseState::LEFTCLICK;
//    if(key == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
//        mouseState = MouseState::RIGHTCLICK;


    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);

    if (key == GLFW_KEY_W) {
        campos += camdir;
    }
    if (key == GLFW_KEY_S){
        campos -= camdir;
    }
    if (key == GLFW_KEY_A) {
        campos -= glm::cross(camdir, camup);
    }
    if (key == GLFW_KEY_D) {
        campos += glm::cross(camdir, camup);
    }
    if (key == GLFW_KEY_UP) {
        camdir += camup*0.01f;
    }
    if (key == GLFW_KEY_DOWN) {
        camdir -= camup * 0.01f;
    }
    if (key == GLFW_KEY_LEFT) {
        camdir -= glm::cross(camdir, camup)*0.01f;
    }
    if (key == GLFW_KEY_RIGHT) {
        camdir += glm::cross(camdir, camup)*0.01f;
    }
    camdir = glm::normalize(camdir);
}

// The MAIN function, from here we start the application and run the game loop
int main()
{
    std::cout << "Starting GLFW context, OpenGL 3.3" << std::endl;
    // Init GLFW
    glfwInit();
    // Set all the required options for GLFW
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    // Create a GLFWwindow object that we can use for GLFW's functions
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "Triangle", nullptr, nullptr);
    if (window == nullptr)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
//    glEnable(GL_DEPTH_TEST);

    // Set the required callback functions
    glfwSetKeyCallback(window, key_callback);

    // Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions
    glewExperimental = GL_TRUE;
    // Initialize GLEW to setup the OpenGL Function pointers
    if (glewInit() != GLEW_OK)
    {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Define the viewport dimensions
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    glViewport(0, 0, width, height);

    // Build and compile our shader program
    // Vertex shader

    GLuint shader = loadSHADER("../res/v2.shader", "../res/f2.shader");
//    GLuint shader = loadSHADER("../res/vertex.shader", "../res/fragment.shader");
    glUseProgram(shader);

    model male_human = model("../res/MaleLow.obj");
//    model car = model("../res/BMW_M3_GTR.obj");
    model triangle = model("../res/triangle.obj");
//    model millenium_falcon = model("../res/millenium-falcon.obj");


    GLuint m_loc = glGetUniformLocation(shader, "model");
    GLuint v_loc = glGetUniformLocation(shader, "view");
    GLuint p_loc = glGetUniformLocation(shader, "pers");
    GLuint c_pos = glGetUniformLocation(shader, "cpos");


    glm::mat4 pers_matrix = glm::perspective(glm::radians(45.f), 1.f, 0.1f, 1000.f);
    glPointSize(5.0f);
    // Game loop
    while (!glfwWindowShouldClose(window))
    {
        //c++;
        // Check if any events have been activated (key pressed, mouse moved etc.) and call corresponding response functions
        glfwPollEvents();

        // Render
        // Clear the colorbuffer
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glm::mat4 view_matrix = glm::lookAt(campos, campos+camdir, camup);
        glUniformMatrix4fv(v_loc, 1, GL_FALSE, &view_matrix[0][0]);
        glUniformMatrix4fv(p_loc, 1, GL_FALSE, &pers_matrix[0][0]);



        model_matrix = rotate_matrix * translate_matrix * scale_matrix * glm::mat4(1.0f);
        glUniformMatrix4fv(m_loc, 1, GL_FALSE, &model_matrix[0][0]);
        glUniform3fv(c_pos, 1 ,&campos[0]);

        auto points = std::vector<DRAW>();
        points.emplace_back(DRAW::FACES);
//        points.emplace_back(DRAW::LINES);
//        points.emplace_back(DRAW::POINTS);


        triangle.draw(points);
        male_human.draw(points);
//        car.draw(points);
//        millenium_falcon.draw(points);

//        model_matrix = glm::mat4(1.0f);
//        translate_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, -10, 0));
//        glm::mat4 inv_trans_matrix = glm::translate(glm::mat4(1.0f), glm::vec3(0, 10, 0));
//        scale_matrix = glm::scale(glm::mat4(1.0f), glm::vec3(1, 1, 1));
//        rotate_matrix = glm::rotate(glm::mat4(1.0f), glm::radians((float)c), glm::vec3(0, 0, 1));
//        model_matrix = inv_trans_matrix * rotate_matrix * translate_matrix * scale_matrix * model_matrix;
//        model_matrix = scale_matrix * model_matrix;
//        glUniformMatrix4fv(m_loc, 1, GL_FALSE, &model_matrix[0][0]);

//        glBindVertexArray(male_human.get_VAO());//HUMAN
//        glDrawArrays(GL_POINTS, 0, male_human.get_size());


        glBindVertexArray(0);


        // Swap the screen buffers
        glfwSwapBuffers(window);
    }

    // Terminate GLFW, clearing any resources allocated by GLFW.
    glfwTerminate();
    return 0;
}
