//
// Created by roman on 9/30/18.
//

#ifndef OPENGLTEST1_MODEL_H
#define OPENGLTEST1_MODEL_H

#include <glm.hpp>
#include "GL/glew.h"	// include GL Extension Wrangler
#include "GLFW/glfw3.h"	// include GLFW helper library

#include <vector>
#include <string>


enum DRAW{
    POINTS,
    LINES,
    FACES
};


class model {

public:
    model(std::string path);
    ~model() = default;

    std::vector<glm::vec3> get_vertices();
    GLuint get_VAO();
    GLuint get_VBO();
    unsigned long get_size();

    void draw(std::vector<DRAW> draw_type);


private:
    static unsigned int model_count;

    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> uv;
    GLuint VAO;
    GLuint VBO;
};


#endif //OPENGLTEST1_MODEL_H
