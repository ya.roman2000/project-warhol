#version 330 core

layout(location = 0) in vec3 position;
layout (location = 1) in vec3 norms;

uniform mat4 model;
uniform mat4 view;
uniform mat4 pers;
uniform vec3 cpos;

out vec3 normal;
out vec3 fragment_position;
out vec3 camera_position;
void main()
{

    gl_Position = pers * view * model * vec4(position, 1.0);
	normal = vec3(transpose(inverse(model)) * vec4(norms, 1));
	fragment_position = vec3(model * vec4(position, 1));
    camera_position = vec3(cpos);

}

