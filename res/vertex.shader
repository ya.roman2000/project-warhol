
#version 330 core
  
layout(location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 pers;

out vec3 col;

void main()
{
    gl_Position = pers * view * model * vec4(position.x, position.y, position.z, 1.0);
	if (color == vec3(0, 0, 0)) {
		col = position;
	}
	else {
		col = color;
	}
	
}
