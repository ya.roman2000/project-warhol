#version 330 core

out vec4 color;
in vec3 normal;
in vec3 fragment_position;
in vec3 camera_position;

void main()
{
	vec3 object_color = vec3(1, 0, 0);

	vec3 light_position = vec3(0, 20, 5);
	vec3 light_color = vec3(0.8, 0.8, 0.8);
    float amb = 0.25f;
    float spec = 1.0f;

	vec3 light_direction = light_position - fragment_position;
    vec3 viewDir = normalize(camera_position - fragment_position);
    vec3 reflectDir = reflect(-light_direction, normal);


	vec3 ambient = amb * light_color;

	float diff = max(dot(normal, light_direction),0.0);
	vec3 diffuse = diff * light_color;

    vec3 specular = spec * pow(max(dot(viewDir, reflectDir), 0.0), 32) * light_color;

	color = vec4((ambient + diffuse + specular) * object_color, 1.0f);
	//color = vec4(normal ,1.0f);
	//color = vec3(1.0f,1.0f,1.0f,1.0f);
}
