# COMP 371 Assignment 2
#### by Roman Taraschi 26776434

# Sources
* Lecture Notes


# Criteria
* The minimum version of OpenGL should be 3.1 and up.
* OpenGL must be used in retained mode.
* Include comments throughout the source code to explain each command/block of commands.
* Load the OBJ files. In this assignment we will be using only the points ***vertices*** to represent the object.
* Prepare all necessary OpenGL data structures to draw the points. Each vertex position will be the (x, y, z) values read from the OBJ file.
* Create a GLFW window of size 800 × 800 with double buffering support.
* Render the object on display.
* The application should use a perspective view to display the object and use the depth buffer for hidden surface removal.
* The application must handle the following input:
    * the user can move the camera using the mouse 
        * moving forward/backward while left button is pressed → move into/out of the scene
    * the user can move the camera using the keyboard 
        * W moves forward, 
        * S moves backwards, 
        * A moves left, 
        * D moves right, 
        * left arrow rotates the camera left about the up vector ← RupL ,
        * right arrow rotates the camera right about the up vector → RupR , 
        * up arrow rotates the camera upwards about the right vector → RrightU,
        * down arrow rotates the camera downwards about the right vector → RrightD
    * the user can rotate the object using keyboard input 
        * B rotates the object about the X axis, 
        * N rotates the object about the Y axis, 
        * E rotates the object about the Z axis
    * the user can move the object using keyboard input 
        * J moves the object along the +X axis, 
        * L moves the object along the -X, 
        * I moves the object along the +Y axis, 
        * K moves the object along the -Y, 
        * PgUp moves the object along the +Z axis, 
        * PgDown moves the object along the -Z axis
    * the user can uniformly scale the object using keyboard input 
        * O scales up the object by a factor of 10%,
        * P scales up the object by a factor of -10%
        
# Sources


